---
title: Khajag presented in Byblos
---
Khajag Apelian presented about his typeface [Arek](http://rosettatype.com/Arek) during a conference organized by the Lebanese American University in Byblos (Lebanon). Arek was also presented at the accompanying exhibition at the Byblos Cultural Center.
