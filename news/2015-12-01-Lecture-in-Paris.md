---
title: Lecture in Paris
---
David Březina gave a lecture about our most recent projects at an event called *Du monde entier au cœur du monde* organized by [Campus Fonderie de l’Image à Bagnolet](http://www.campusfonderiedelimage.org/mediatheque/colloque-typo-fontes-et-caracteres-dans-tous-leurs-etats-du-monde-entier-au-coeur-du) in Paris. Small, but really nice event, audience, and colleagues presenting. You can see some [photos on flickr](https://www.flickr.com/photos/campusfonderiedelimage/sets/72157662134031945). Thank you for having us!