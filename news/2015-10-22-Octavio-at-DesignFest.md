---
title: Octavio at DesignFest 2015
---
Octavio Pardo presented about his design work at the ninth [DesignFest](http://www.design-fest.com) in Guadalajara (Mexico) to an audience of over 1400 attendees. Here is a picture of [him, larger than life Sutturah letters, and an excessive amount of smileys](https://twitter.com/malenybravo/status/657964159867326464).
