# README – Blog

This folder is tracked with Git for website updates.

Note that "_ source" folders are not tracked by Git.

## Width of images on the blog

Images have to have this dimensions before upload. They are not rescaled or otherwise optimized.

Normal: 610px (or 2x for retina displays)
Big (with _big added to the name): 940px (or 2x for retina displays)


## Opengraph (OG) images for social networks

Add this kind of code between the front matter (stuff between ---) and the content in order to get images which are not included in the article, but might be used by social networks when sharing a post. Useful, esp. when SVGs are used in the text as SVGs cannot be used on social networks.

=OG=IMG=
{{ blog.url }}/assets/2015-Considerations-in-multilingual-type-design/OG_Gujarati-sans.png
{{ blog.url }}/assets/2015-Considerations-in-multilingual-type-design/OG_Gujarati-foot.png
=OG=END=