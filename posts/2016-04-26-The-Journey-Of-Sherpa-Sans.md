---
title: The Journey Of Sherpa Sans
author: William Montrose
tags:
  - release
  - foundry news
excerpt: Sherpa Sans is a flared sans serif, reminiscent of engraving and stone carving. Sturdy and informal, the design features a moderate contrast with distinct terminals.
---

=OG=IMG=
{{ blog.url }}/assets/2016-The-Journey-Of-Sherpa-Sans/OG_SherpaSans_preview.png
=OG=END=

[Sherpa Sans](http://rosettatype.com/SherpaSans) is Florian Runge’s debut release – a flared sans serif, reminiscent of engraving and stone carving. Sturdy and informal, the design features a moderate contrast with distinct terminals.

The idea of a raw typeface was explored by Rudolf Koch and Berthold Wolpe, as early as the 1920s. Experiments with shapes that negotiate boundaries between the exactness of mechanical production and the imperfection of human condition led to the development of designs like Neuland and the ubiquitous Albertus. Nonetheless, they were limited to display typography, namely book jackets, and are not suitable for continuous reading.

Sherpa takes expressive forms derived from chiseling and engraving and places them comfortably on the crossroads of editorial and package design. A contemporary typeface with a wide range of applications, that doesn’t fall into the anachronistic traps, set by similar genres.

The genre-less-ness turned out to be the ultimate design challenge. Balancing functionality and expression leads to questions of categorization. Is it really a serif or a sans? Is it a display or text typeface? Strong features like the cuneiform head serif on characters like *n* and *m*, or the deeply cut wedge terminals on *c*, *r*, and *s* give Sherpa a sculptural appeal – a quality that is desired for all things display. A rhythmic pattern and a classic construction make it sparkle in text. You will have to decide, we clearly could not.

Sherpa’s versatility is underpinned by a huge typographic repertoire. Apart from supporting over 120 languages, the fonts contain small caps, six sets of numerals, including case-sensitive figures, and a vast array of Open-Type features.

![]({{ blog.url }}/assets/2016-The-Journey-Of-Sherpa-Sans/SherpaSans-previews_big.svg)

