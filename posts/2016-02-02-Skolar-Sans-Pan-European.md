---
title: Skolar Sans Pan-European
tags:
  - release
  - foundry news
  - Cyrillic
  - Greek
  - insight
excerpt: Last year, we extended Skolar Sans to cover not just Latin but also Greek and Cyrillic scripts. The fonts are now served to millions of readers of Radio Free Europe/Radio Liberty and associated news portals.
---

=OG=IMG=
{{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/OG_Skolar-Sans-PE-infographic.png
=OG=END=

### Expanding the design space

Skolar Sans is Rosetta’s biggest type system so far, and as such, a good study in a design expansion. The family consists of 72 styles and addresses the needs of modern responsive design. It is able to adapt to a wide range of devices and formats, from the biggest desktop screen to the smallest printed booklet. Last year, Sláva Jevčinová and me started extending Skolar Sans to full pan-European standard so it would cover not just Latin but also Greek and Cyrillic scripts. After going from serif to sans, and to a great number of widths, weights, and styles, there was still room for another parameter – language.

![]({{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/Design-parameters_big.svg)


### A truly European project

The advanced language support was commissioned by [Radio Free Europe/Radio Liberty](http://www.rferl.mobi) with the goal to carry their content through all of their newly developed websites (together with our Skolar PE and Merriweather by Eben Sorkin). The project, one of very few to support this many languages on the same CMS platform, was headed by Kim Conger, Design Director at RFE/RL and Karel Knop, who brought it to life in his role as the Senior Designer. The platform, [Pangea](http://www.pangea-cms.com), supports RFE/RL websites as well as Voice of America, Middle East Broadcasting and Cuban Broadcasting. Every day, 95 responsive sites are accessed by 1.3 million users (on average) from 200 countries and territories. RFE/RL has launched the mobile and tablet versions (see [Radio Ozodi](http://www.ozodi.mobi) in Tajik for example) and yesterday they started rolling out the desktop versions as well (see [Radio Evropa e Lirë](http://www.evropaelire.org/) in Albanian for example).

![]({{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/Ozodi-web.jpg)

The plans to include more scripts in Skolar Sans existed before but sometimes it takes a pressing need and a visionary client to get things going. We were happy to work with Irene Vlachou and Maxim Zhukov on this project, assisting us with Greek and Cyrillic, respectively. The resulting linguistic scope, together with the number of styles, is largely unprecedented and we would not have achieved this quality without our consultants.

Although we constantly strive to release large multilingual projects, putting this much work and effort into a single product presents a challenge for a small business like us, as great risk comes with designing for an unpredictable market. The fact that additional language and script support is now increasingly requested by clients, might tell us something about the changing demands in delivering content today. Consequently, briefs like this allow us to do our work in the best possible environment.

![]({{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/Preview_big.svg)


### The unwritten laws of language extension

Extending Skolar Sans to include Cyrillic and Greek meant adding extra 1000 glyphs to each font. The Cyrillic supports many Slavic and Asian languages, but also comes equipped with language-sensitive character variants for Macedonian, Serbian, and Bulgarian. The Greek offers support for monotonic as well as polytonic orthography. Monotonic Greek is used in everyday communication and is the successor of polytonic Greek, which is necessary for academic publications and a variety of conservative texts. Altogether, the type system will effortlessly provide support for at least 140 languages. We also added 300 more glyphs that are used in linguistics and for various Latin transliterations of Asian scripts (e.g. Chinese, Indian languages including Sanskrit, Arabic, Hebrew). That brings Skolar Sans PE on a par with our popular text type family Skolar PE, which has only a slightly bigger glyph repertoire which we hope to match eventually.

![Comparison of the default, International Cyrillic and Bulgarian letterforms in uprights (top, Bulgarian in Green) and default and Serbian/Macedonian forms in uprights and italics (bottom, Serbo-macedonian in Green)]({{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/Bulgarian-Serbian-Cyrillic_big.svg)


### A multiverse design approach

A type system is designed to function in a universal way, although in this case the design itself cannot follow a single recipe. Like with culture, what works for the Latin might not be the best solution for other scripts. The cursive Greek is a good example for that. Greek has a very fluid motion, and is not as rigid as Latin or Cyrillic. Hence, the distinction between upright and italic styles would not be as strong if they followed the same construction, so we decided to use more of the significantly different forms for the cursive Greek and introduce various shape subtleties to distinguish the two. That way, the cursive maintains similar degree of typographic versatility as Latin and Cyrillic, while following a different design approach.

![Comparison of Latin and Greek and their corresponding italics. Marked green are characters that change their construction beyond mere sloping.]({{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/Italics-comparison_big.svg)


### Font size matters

If you care about your users’ experience, you care about your website performance. And with RFE/RL’s mission to broadcast to millions of users in so many countries with slow or censored internet this becomes even more of an issue. Keeping the loading speed high (and the size of the websites small) sets a strict limit on fonts. Rosetta’s designers were facing a double challenge: supporting a large number of languages in three different scripts that are bound to result in large fonts, while reducing the size of those fonts drastically, so that they can be used on the web. In short, we went from around 268KB fonts to 33–40KB subsetted webfonts, and it was not very easy. Elaborating on the nitty-gritty would require another article.

<br>

<a href="https://rosettatype.com/skolarsans" class="red solidButton">
<span class="innerText">Study Skolar Sans in more detail</span>
</a>

### Skolar Sans in Numbers

171 396 glyphs, 3600+ hours of drawing and engineering, 2370–2391 glyphs per font, 143+ languages covered, 72 styles, 10 months development time, sanity lost and recovered: more than three times, all of that with Daft Punk’s Tron Legacy soundtrack on repeat with [Stella Carnacina and Daniela Poggi](https://soundcloud.com/bordelloaparigi/flemming-dalum-hot-girls-of) to lift up the spirits on a particularly rainy English day.

*Having been born in communist Czechoslovakia where RFE was one of the beacons of uncensored news, it has been real honour to contribute back to RFE/RL with our fonts. And now that they are widely available we hope they will serve our retail customers equally well.*

<br>

![]({{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/Glyphs-and-languages_big.svg)

<br>

![]({{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/Commits_big.svg)

<br>

![]({{ blog.url }}/assets/2016-Skolar-Sans-Pan-European/Email-estimates_big.svg)